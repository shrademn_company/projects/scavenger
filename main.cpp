//------------------------------------------------------------------------------
//
// main.cpp created by Yyhrs 2021/06/23
//
//------------------------------------------------------------------------------

#include <SApplication.hpp>

#include "MainWindow.hpp"

int main(int argc, char *argv[])
{
	SApplication application{argc, argv};
	MainWindow   window;

	window.show();
	return SApplication::exec();
}
