//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2021/06/23
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QFileInfo>
#include <QMovie>
#include <QStringListModel>

#include <Map.hpp>
#include <Settings.hpp>
#include <TableModel.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum Data
	{
		Path = Qt::UserRole,
		Name,
		Planet,
		Type,
		Dimension,
		Layers,
		Environments,
		Heightmap,
		ItemCounts
	};
	enum Column
	{
		Map = 0,
		Preview,
		AI,
		State
	};
	Q_ENUM(Column)
	enum SettingsKey
	{
		ExtractEnvironments,
		ExtractPreview,
		RemovePreview,
		ExportHeightmap,
		Items
	};
	Q_ENUM(SettingsKey)

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

	bool eventFilter(QObject *watched, QEvent *event) override;

private:
	struct Information
	{
		int previewPosition{0};
		int previewSize{0};
	};

	void connectActions();
	void connectWidgets();

	void                   addMaps(const QList<Alamo::Map> &maps);
	void                   removeMap(QFileInfo const &file);
	void                   updateDetails(int row);
	QImage                 getHeightmap(const Alamo::Map::Data &data) const;
	QMap<quint32, QString> getItems() const;
	void                   processFile(int row);

	static void        extractEnvironments(QFileInfo const &source, const QList<Alamo::Map::Environment> &environments, QDir const &destination);
	static Information retrieveInformation(QFileInfo const &source);
	static void        extractPreview(QFileInfo const &source, Information const &information, QFileInfo const &destination);
	static void        removePreview(QFileInfo const &source, Information &information);

	static inline Alamo::Map map(QFileInfo const &file)
	{
		return Alamo::Map{file};
	}

	Settings         m_settings;
	QMovie           m_jawa;
	QStringListModel m_items;
	TableModel       m_maps;
	QImage           m_currentHeightmap;
};

#endif // MAINWINDOW_HPP
