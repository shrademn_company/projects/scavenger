//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2021/06/23
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QDropEvent>
#include <QFileDialog>
#include <QMimeData>
#include <QtConcurrent>

#include <core.tpp>
#include <ChunkFile.hpp>
#include <operators.hpp>
#include <SApplication.hpp>
#include <utils.hpp>

#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent},
	m_jawa{":/jawa.gif"}
{
	auto items{m_settings.value(Items).toStringList()};

	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	items.removeDuplicates();
	items.sort();
	m_items.setStringList(items);
	itemListView->setModel(&m_items);
	m_maps.addColumns(QList{Map, Preview, AI, State});
	mapTableView->setModel(&m_maps);
	mapTableView->horizontalHeader()->setSectionResizeMode(Map, QHeaderView::Stretch);
	mapTableView->horizontalHeader()->setSectionResizeMode(AI, QHeaderView::Stretch);
	mapTableView->horizontalHeader()->setSectionResizeMode(State, QHeaderView::Stretch);
	mapTableView->installEventFilter(this);
	movieLabel->setMovie(&m_jawa);
	movieLabel->hide();
	m_settings.restoreState(this);
	extractEnvironmentsCheckBox->setChecked(m_settings.value(ExtractEnvironments).toBool());
	extractPreviewCheckBox->setChecked(m_settings.value(ExtractPreview).toBool());
	removePreviewCheckBox->setChecked(m_settings.value(RemovePreview).toBool());
	exportHeightmapCheckBox->setChecked(m_settings.value(ExportHeightmap).toBool());
	connectActions();
	connectWidgets();
	connect(&m_jawa, &QMovie::frameChanged, this, [this](int frameNumber)
	{
		if (frameNumber == m_jawa.frameCount() - 1)
		{
			m_jawa.stop();
			movieLabel->hide();
		}
	});
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(SApplication::Theme, QIcon::themeName());
	m_settings.setValue(ExtractEnvironments, extractEnvironmentsCheckBox->isChecked());
	m_settings.setValue(ExtractPreview, extractPreviewCheckBox->isChecked());
	m_settings.setValue(RemovePreview, removePreviewCheckBox->isChecked());
	m_settings.setValue(ExportHeightmap, exportHeightmapCheckBox->isChecked());
	m_settings.setValue(Items, m_items.stringList());
}

bool MainWindow::eventFilter(QObject */*watched*/, QEvent *event)
{
	if (event->type() == QEvent::DragEnter)
	{
		auto *dragEnterEvent{dynamic_cast<QDragEnterEvent *>(event)};

		for (auto const &url: adapt(dragEnterEvent->mimeData()->urls()))
		{
			QFileInfo file{url.toLocalFile()};

			if (file.exists() && file.suffix().compare("ted", Qt::CaseInsensitive) == 0)
			{
				event->accept();
				return true;
			}
		}
	}
	else if (event->type() == QEvent::Drop)
	{
		auto          *dropEvent{dynamic_cast<QDropEvent *>(event)};
		QFileInfoList files;

		for (auto const &url: adapt(dropEvent->mimeData()->urls()))
		{
			QFileInfo file{url.toLocalFile()};

			if (file.suffix().compare("ted", Qt::CaseInsensitive) == 0)
			{
				files << file;
				removeMap(file);
				event->accept();
			}
		}
		addMaps(QtConcurrent::blockingMapped<QList<Alamo::Map>>(files, map));
		return true;
	}
	return false;
}

void MainWindow::connectActions()
{
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
	addItemToolButton->setDefaultAction(actionAddItems);
	connect(actionAddItems, &QAction::triggered, this, [this]
	{
		auto items{m_items.stringList()};

		if (m_items.stringList().contains(itemLineEdit->text().toUpper()))
			return ;
		items << itemLineEdit->text().toUpper();
		m_items.setStringList(items);
		m_items.sort(0);
	});
	removeItemToolButton->setDefaultAction(actionRemoveItems);
	itemListView->addAction(actionRemoveItems);
	connect(actionRemoveItems, &QAction::triggered, this, [this]
	{
		QList<int> rows;

		for (auto const &index: adapt(itemListView->selectionModel()->selectedRows()))
			rows << index.row();
		std::sort(rows.begin(), rows.end(), std::greater<int>());
		for (auto const &row: qAsConst(rows))
			m_items.removeRow(row);
	});
	mapTableView->addAction(actionRemoveMaps);
	connect(actionRemoveMaps, &QAction::triggered, this, [this]
	{
		m_maps.removeRows(mapTableView->logicalSelectedRows());
	});
	actionSave->setDisabled(true);
	heightmapLabel->addAction(actionSave);
	connect(actionSave, &QAction::triggered, this, [this]
	{
		QFileInfo map{pathLineEdit->text()};
		QString   path{QFileDialog::getSaveFileName(this, map.completeBaseName(), map.absolutePath() / map.completeBaseName(), "Heightmap (*.png)")};

		if (path.isEmpty())
			return ;
		m_currentHeightmap.save(path);
	});
}

void MainWindow::connectWidgets()
{
	connect(itemListView->selectionModel(), &QItemSelectionModel::selectionChanged, this, [this](QItemSelection const &selected, QItemSelection const & /*deselected*/)
	{
		removeItemToolButton->setDisabled(selected.indexes().isEmpty());
	});
	connect(itemLineEdit, &QLineEdit::textChanged, this, [this]
	{
		addItemToolButton->setDisabled(itemLineEdit->text().isEmpty());
	});
	connect(mapTableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, [this](QItemSelection const &selected, QItemSelection const & /*deselected*/)
	{
		actionRemoveMaps->setDisabled(selected.indexes().isEmpty());
		if (!selected.indexes().isEmpty())
			updateDetails(mapTableView->logicalSelectedRows().constFirst());
	});
	connect(scavengePushButton, &QPushButton::clicked, this, [this]
	{
		m_jawa.start();
		movieLabel->show();
		for (int row: adapt(m_maps.rows()))
			processFile(row);
	});
}

void MainWindow::addMaps(const QList<Alamo::Map> &maps)
{
	for (auto const &map: maps)
	{
		QMap<quint32, int> objects;

		m_maps.addRows(1);
		m_maps.setData(m_maps.rowCount() - 1, Map, map.file().completeBaseName());
		m_maps.setData(m_maps.rowCount() - 1, Map, map.file().absoluteFilePath(), Path);
		m_maps.setData(m_maps.rowCount() - 1, Map, map.properties().name, Name);
		m_maps.setData(m_maps.rowCount() - 1, Map, map.properties().planet, Planet);
		m_maps.setData(m_maps.rowCount() - 1, Map, (map.properties().type == Alamo::MAP_LAND ? QStringLiteral("Land") : QStringLiteral("Space")), Type);
		m_maps.setData(m_maps.rowCount() - 1, Map, map.properties().dimension, Dimension);
		m_maps.setData(m_maps.rowCount() - 1, Map, QVariant::fromValue(map.layers()), Layers);
		m_maps.setData(m_maps.rowCount() - 1, Map, QVariant::fromValue(map.environments()), Environments);
		m_maps.setData(m_maps.rowCount() - 1, Map, QVariant::fromValue(map.heightmap()), Heightmap);
		for (auto const &object: map.objects())
			objects[object.crc] += 1;
		m_maps.setData(m_maps.rowCount() - 1, Map, QVariant::fromValue(objects), ItemCounts);
		m_maps.setData(m_maps.rowCount() - 1, Preview, map.preview());
		m_maps.setData(m_maps.rowCount() - 1, AI, (map.ai().isEmpty() ? QStringLiteral("Good") : map.ai()));
		if (!map.ai().isEmpty())
			m_maps.setData(m_maps.rowCount() - 1, AI, QColor{SApplication::s_colorRed}, Qt::BackgroundRole);
		m_maps.setData(m_maps.rowCount() - 1, State, (map.error().isEmpty() ? QStringLiteral("Good") : map.error()));
		if (!map.error().isEmpty())
			m_maps.setData(m_maps.rowCount() - 1, State, QColor{SApplication::s_colorRed}, Qt::BackgroundRole);
	}
}

void MainWindow::removeMap(QFileInfo const &file)
{
	for (int row: adapt(m_maps.rows()))
		if (m_maps.data(row, Map, Path).toString() == file.absoluteFilePath())
		{
			m_maps.removeRows({row});
			return ;
		}
}

void MainWindow::updateDetails(int row)
{
	static QString const format{QStringLiteral("%1: %2")};
	static auto          dimension{[](QSizeF const size)
		{
			static QString const format{QStringLiteral("%1 x %2")};

			return format.arg(size.width()).arg(size.height());
		}};
	auto const items{getItems()};

	pathLineEdit->setText(m_maps.data(row, Map, Path).toString());
	nameLineEdit->setText(m_maps.data(row, Map, Name).toString());
	planetLineEdit->setText(m_maps.data(row, Map, Planet).toString());
	typeLineEdit->setText(m_maps.data(row, Map, Type).toString());
	dimensionLineEdit->setText(dimension(m_maps.data(row, Map, Dimension).toSizeF()));
	m_currentHeightmap = getHeightmap(m_maps.data(row, Map, Heightmap).value<Alamo::Map::Data>());
	actionSave->setDisabled(m_currentHeightmap.isNull());
	heightmapLabel->clear();
	heightmapLabel->setPixmap(QPixmap::fromImage(m_currentHeightmap));
	heightmapLabel->setScaledContents(true);
	textureListWidget->clear();
	for (auto const &layer: m_maps.data(row, Map, Layers).value<QList<Alamo::Map::Layer>>())
	{
		QStringList textures;

		if (!layer.colorTexture.isEmpty())
			textures << layer.colorTexture;
		if (!layer.normalTexture.isEmpty())
			textures << layer.normalTexture;
		if (!textures.isEmpty())
			textureListWidget->addItem(textures.join(", "));
	}
	textureListWidget->sortItems();
	itemCountListWidget->clear();
	for (auto const &&[crc, count]: keyValues(m_maps.data(row, Map, ItemCounts).value<QMap<quint32, int>>()))
		if (items.contains(crc))
			itemCountListWidget->addItem(format.arg(items[crc]).arg(count));
	itemCountListWidget->sortItems();
}

QImage MainWindow::getHeightmap(const Alamo::Map::Data &data) const
{
	QImage image{data.size, QImage::Format_RGBA8888};
	int    index{0};

	if (data.maximumElevation == data.minimumElevation)
		return {};
	for (auto value: data.elevationMap)
	{
		int    intensity{static_cast<int>(static_cast<float>(value - data.minimumElevation) / (data.maximumElevation - data.minimumElevation) * 255)};
		QColor color{intensity, intensity, intensity};

		image.setPixelColor(index % data.size.width(), index / data.size.width(), color);
		++index;
	}
	return image.mirrored();
}

QMap<quint32, QString> MainWindow::getItems() const
{
	QMap<quint32, QString> items;

	for (auto const &item: adapt(m_items.stringList()))
	{
		auto value{item.toLatin1()};

		items[Alamo::CRC32(value.data(), value.size())] = item;
	}
	return items;
}

void MainWindow::processFile(int row)
{
	if (m_maps.data(row, State, Qt::BackgroundRole).value<QColor>().isValid())
		return ;
	else
	{
		QFileInfo   source{m_maps.data(row, Map, Path).toString()};
		Information information{retrieveInformation(source)};

		if (extractEnvironmentsCheckBox->isChecked())
			extractEnvironments(source, m_maps.data(row, Map, Environments).value<QList<Alamo::Map::Environment>>(), source.absolutePath() / source.completeBaseName());
		if (information.previewSize && extractPreviewCheckBox->isChecked())
			extractPreview(source, information, source.absolutePath() / source.completeBaseName() + ".tga");
		if (information.previewSize && removePreviewCheckBox->isChecked())
			removePreview(source, information);
		if (exportHeightmapCheckBox->isChecked())
		{
			auto heightmap{m_maps.data(row, Map, Heightmap).value<Alamo::Map::Data>()};

			if (heightmap.size.width() && heightmap.size.height())
				getHeightmap(heightmap).save(source.absolutePath() / source.completeBaseName() + ".png");
		}
	}
}

void MainWindow::extractEnvironments(QFileInfo const &source, const QList<Alamo::Map::Environment> &environments, QDir const &destination)
{
	QFile file{source.absoluteFilePath()};

	file.open(QFile::ReadOnly);
	destination.mkpath(".");
	for (auto const &environment: environments)
	{
		QFile tee{destination / QString{environment.name} + ".tee"};

		file.seek(environment.position - sizeof(Alamo::ChunkHeader));
		tee.open(QFile::Truncate | QFile::WriteOnly);
		tee.write(file.read(environment.size + sizeof(Alamo::ChunkHeader)));
	}
}

MainWindow::Information MainWindow::retrieveInformation(QFileInfo const &source)
{
	Alamo::ChunkFile reader{source};
	auto             type{reader.next()};
	Information      information;

	while (type != -1)
	{
		if (type == Alamo::Map::ChunkPreview)
		{
			information.previewPosition = reader.fileTell();
			information.previewSize = reader.size();
			return information;
		}
		type = reader.next();
	}
	return information;
}

void MainWindow::extractPreview(QFileInfo const &source, Information const &information, QFileInfo const &destination)
{
	QFile file{source.absoluteFilePath()};
	QFile preview{destination.absoluteFilePath()};

	file.open(QFile::ReadOnly);
	file.seek(information.previewPosition);
	preview.open(QFile::Truncate | QFile::WriteOnly);
	preview.write(file.read(information.previewSize));
}

void MainWindow::removePreview(QFileInfo const &source, Information &information)
{
	QFile      file{source.absoluteFilePath()};
	QByteArray content;

	file.open(QFile::ReadOnly);
	content.append(file.read(information.previewPosition - sizeof(uint32_t) * 2));
	file.skip(information.previewSize + sizeof(uint32_t) * 2);
	content.append(file.readAll());
	file.close();
	file.open(QFile::Truncate | QFile::WriteOnly);
	file.write(content);
}
